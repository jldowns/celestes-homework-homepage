(demo-section)=
# Online Demonstration

You can try an online demo of Celeste's Homework at [c-demo.jldowns.com](https://c-demo.jldowns.com).

```{note}
The demo server is redeployed every hour, on the hour. If you get a "502 Bad Gateway" error from nginx, wait a few minutes and try again.
```

```{note}
Celeste's Homework is intended to teach kids to use a keyboard. It won't work with a phone or tablet unless you have a bluetooth keyboard connected.
```

No one has volunteered their family videos for the public demo yet, so I used public domain PSA videos. It doesn't make the point the same way actual personal videos would, but it at least shows the technology. [Video credits are listed below](demo-credits).

This is a fully functioning demo of the platform, meaning uploads and database operations work. The demo runs in a virtual machine that is reset every hour (so don't worry too much about messing up the database).

Try exploring the demo like a child would, without instructions. Then come back and [read the usage guide](usage-section) to find the features you missed and walk through the administration options.


(demo-credits)=
## Demo Video Credits

* *Alone at Home* by Alfred Higgens Productions [(via the Prelinger Archives)](https://archive.org/details/aloneathome)
* *American Look* by Handy (Jam) Organization [(via the Prelinger Archives)](https://archive.org/details/American1958)
* *Computer Calculator for Math and Science* by Hewlett Packard [(via the Prelinger Archives)](https://archive.org/details/0808_Computer_Calculator_for_Math_and_Science_05_28_16_00)
* *thumbs up,a-ok* by Center for Accessible Technology in Sign [(via the Prelinger Archives)](https://archive.org/details/thumbsupa-okASL2)
