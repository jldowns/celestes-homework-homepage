<!-- #region -->
# Getting Started

## Deploy with Docker

Every new release is built as a docker image and uploaded to project's [Gitlab container registery](https://gitlab.com/jldownsgroup/celestes-homework/container_registry). Both x86 and ARM images are built. Gitlab doesn't have native support for architecture selection yet so the architecture is included in the tag, along with the version.

```bash
# To run version 0.9 on an x86 processor:
docker run -p 3000:3000 jldownsgroup/celestes-homework:0.9-x86

# To run version 0.9 on an ARM processor:
docker run -p 3000:3000 jldownsgroup/celestes-homework:0.9-arm 
    ...
```

Optionally you can add the following options:

If you want to include [your own voice speaking the numbers and letters](letter-setup), you can include them with these volumes:
```
-v ./my_letter_sounds:/code/public/letter_sounds
-v ./my_math_sounds:/code/public/math_sounds
```

(persist-backups)=
To persist [backups of your video files](backing-up):
```
-v ./backups:/code/backups
```

To maintain the database between deployments and restarts. `/db` contains the SQLite files and `storage` contains the video and sound files blobbed by the database:
```
-v ./db:/code/db
-v ./storage:/code/storage
```

## Deploy From Source

Before deploying from source, make sure the following dependencies are installed:
- node 12
- yarn
- ffmpeg
- Ruby 2.7

Then install and start the Rails server:

```bash
# Clone the repo
git clone https://gitlab.com/jldownsgroup/celestes-homework.git

# Install libraries
bundle install
yarn
rails webpacker:install

# Prepare database
rails db:migrate RAILS_ENV=production

# Update binaries
rake app:update:bin

# Generate an encryption key that's not used for anything but
# is required by Rails 6.
export SECRET_KEY_BASE=\$(rake secret)

# Run production server
rails s -e production
```

(letter-setup)=
## Setting up the "letter and number" voice files

The "letter sounds" are not stored in the database like the "word" sounds and videos. Instead, they are stored as assets in the `public/` folder. If you look in `public/letter_sounds` and `public/math_sounds`, you'll see all the sound files that the site defaults to. You should replace these with recordings of your own voice! I've found that kids really like to hear their own voice here, so record your favorite child  saying her letters and numbers for bonus points.

If you deploy with docker, you can create a volume to set up the files.

```bash
docker run jldownsgroup/celestes-homework:0.9-x86 \
    -v ./my_letter_sounds:/code/public/letter_sounds \
    -v ./my_math_sounds:/code/public/math_sounds
```


(create-vocabulary)=
## Creating the vocabulary and uploading videos

It was my experience that once family and friends saw my daughter using this program, they wanted *their* name and video to be programmed in. The web interface was added so that family and friends with access to the website can add videos themselves.

There's no interface button to access the "word administration" console. You simply add `/words` to the url to navigate there.

[Here's the word administration section of the online demo](https://c-demo.jldowns.com/words). Know that this is a fully functioning demo of the platform, meaning uploads and database operations work. The demo runs in a virtual machine that resets every hour, so if you upload any videos they will be deleted within the hour.

### Special Words

There are certain words that are used to navigate to different "sections" of the website. Typing MATH, for example, [will navigate to the math section](math-section-usage). For this navigation to work, MATH needs to be added to the website's vocabulary [as outlined above](create-vocabulary). If this is confusing, try it out on the demo:

First, type MATH [to navigate to the math section](https://c-demo.jldowns.com/).

Then delete the word MATH using the demo's [administration section](https://c-demo.jldowns.com/words).

Then go back to the demo's home page and try typing MATH again.
<!-- #endregion -->

(backing-up)=
## Backing up videos

Adding words and videos [using the web interface](create-vocabulary) stores the word information in an SQLite database. The videos are filed away in a machine-readable place, but unexpected hardware restarts can cause data loss. This can be semi-tragic if family members have added their videos to the website.

By navigating to `/backup`, all videos and sound files will be backed up in a human-readable place. Specifically, they are dumped to the `/backup` folder at the website's root on the server in the following format:

```
/backups
  /words
    /mom
      - mom.mp3
      - mom.mp4
    /emily
      - emily.mp3
      - emily.mp4
     ...
```

If you're running Celeste's Homework using Docker, [creating a volume](persist-backups) in the container at `/code/backups/` will persist these backups.

```{note}
The backup and restore features are in development. Right now there's no progress indicator so the website will act like it's frozen. Just give it a few minutes!
```

## Restoring from backup

Navigating to `/restore` will restore the words, sounds, and videos you backed up in the previous section.

```{note}
The backup and restore features are in development. Right now there's no progress indicator so the website will act like it's frozen. Just give it a few minutes!
```

After the restore is complete, the webpage will display a log output of the videos and sounds that were restored... it's not pretty right now but this is really a webmaster operation rather than a user operation.
