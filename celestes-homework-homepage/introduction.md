# Introduction

Celeste's Homework is a self-hosted learning platform for young children. It is currently aimed at preschool-aged kids who are curious about computers and have been introduced to letters and numbers. Its primary purpose is to teach children the basics of using a keyboard and mouse while also improving spelling and simple math along the way. It has been designed to work on both desktop web browsers and iPad browsers with a bluetooth keyboard.

* [Go to project repository on Gitlab](https://gitlab.com/jldownsgroup/celestes-homework)

* [View online demo](demo-section)

## Philosophy

The ready-to-play learning apps and websites for children never seem to catch my kids' attention. Their strategy is usually explicit gamification of learning (such as an RPG that requires solutions to math problems for mana) or using cute characters to keep childrens' attention.

The three-year-olds I know are pretty social. While they are interested in playing with toys and reading books, their top interest is interacting with their family and friends. There's probably an evolutionary/developmental insight here, but the part that's important to this project is that there's nothing in ready-to-play learning games that really interest social children. They don't care about random characters as much as their family.

This project is simple - allow parents to add their own content. Parents upload every video that plays and every sound that is played. The theory is that a parent saying "good job" is more meaningful than an orange rabbit saying "good job." My experience supports this theory.

What I've found, too, is that in a world with unlimited content and glossy games vying for your kids' attention, a game with their own family in it is incredibly special and comforting to children. They can tell it's different and they will keep coming back to it.

## Overview

Right now there are two "modes" - spelling and math.

The spelling mode is the default. Kids can type anything they like and the letters appear on the screen. If they type a word that is loaded in the platform, a video plays. For example, if they type "DAD", a video of their dad plays.

The math mode shows simple one-digit addition and subtraction problems. Right now the solutions can be one or two digit positive numbers (and zero).
