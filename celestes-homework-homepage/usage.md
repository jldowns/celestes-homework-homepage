(usage-section)=
# Basic Usage

You interact with Celeste's Homework by typing words using the keyboard. It will recognize any words that you program into it. (Adding new words is easy; see the [Creating the Vocabulary section](create-vocabulary) for instructions.) If you type a word in its vocabulary it will say the word and play a video.

## Hints

Since most preschoolers don't know how to spell, there is a big HINT button that shows words alongside thumbnails of the videos. You can click this button with a mouse or tap it on a tablet to toggle the display.

Once the hints are displayed, press the left and right arrow keys on your keyboard to cycle through the vocabulary.

## Special Words

There is support to add different "sections" of the website. Right now there is one other section: the math section. To activate the math section, type MATH on your keyboard.

(math-section-usage)=
### The Math Section

```{note}
Even though the special words are special, they will only be recognized if they are  [added to the program's vocabulary](create-vocabulary). If you don't add "MATH" to the vocabulary, the math section will be inaccessible.
```

The math section chooses single digit numbers, and asks you to add or subtract them. The answer may be a single digit number, a two-digit number, or zero, but it is never negative.

After typing the answer, press Return to submit the answer. Use backspace to delete numbers you've typed.

To return to the spelling section, click the BACK button with your mouse or tap it on a tablet.

## Adding words and videos

There is a simple web interface to add/edit/remove words and videos from the website's vocabulary. See the [Creating the Vocabulary section](create-vocabulary) in the Getting Started chapter for instructions.

