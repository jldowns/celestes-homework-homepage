# Celeste's Homework

This is the documentation for the Celeste's Homework project. This documentation is built using Jupyter Book and hosted on Gitlab pages. The Celeste's Homework project is built with Ruby on Rails.

The Gitlab Pages URL for this documentation is https://jldowns.gitlab.io/celestes-homework-homepage/

The custom URL for this documentation is https://celestes-homework.justindowns.com/




