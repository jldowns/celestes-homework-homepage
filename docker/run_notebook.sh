source /root/.bashrc
conda activate book
echo $CONDA_PREFIX
jupyter notebook --ip='*' --port=8234 --NotebookApp.token='' --NotebookApp.password='' --allow-root --notebook-dir=/root/book/
