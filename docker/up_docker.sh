# Run this script from the `./docker` folder.

VERSION=0.1
REPO=celestes-homework-homepage

cd .. && docker run \
       -it \
       -p 8234:8234 \
       -v $(pwd):/root/book/ \
       $REPO:$VERSION \
       /bin/bash -c "source /root/book/docker/run_notebook.sh"
